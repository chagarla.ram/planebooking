package org.flightBooking.Models;

public class FlightScheduledTravelClass {
    private String flightClass;
    private int flightId;
    private int noOfSeatsForFlightClassAvaliable;
    private int priceBasedOnClass;

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getNoOfSeatsForFlightClassAvaliable() {
        return noOfSeatsForFlightClassAvaliable;
    }

    public void setNoOfSeatsForFlightClassAvaliable(int noOfSeatsForFlightClassAvaliable) {
        this.noOfSeatsForFlightClassAvaliable = noOfSeatsForFlightClassAvaliable;
    }

    public int getPriceBasedOnClass() {
        return priceBasedOnClass;
    }

    public void setPriceOfBasedOnClass(int priceOfBasedOnClass) {
        this.priceBasedOnClass = priceOfBasedOnClass;
    }
    public FlightScheduledTravelClass(){}
    public FlightScheduledTravelClass(int flightId, String seatClass, int noOfSeatsForFlightClassAvaliable,int priceBasedOnClass) {
        this.flightId=flightId;
        this.flightClass = seatClass;
        this.noOfSeatsForFlightClassAvaliable = noOfSeatsForFlightClassAvaliable;
        this.priceBasedOnClass=priceBasedOnClass;
    }

}