package org.flightBooking.Models;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Objects;
import java.util.List;

public class AjaxResponseBody {
        String msg;
        List<FlightData> result;

        public void setResult(List<FlightData> result) {
                this.result = result;
        }

        public List<FlightData> getResult() {
                return result;
        }

}
