package org.flightBooking.Models;
import java.util.ArrayList;

public class Flight {
    private int flightID;
    private String flightName;
    private int flightTotalSeats;
    private String compName;

    public Flight(int flightID, String flightName, int flightTotalSeats) {
        this.flightID = flightID;
        this.flightName = flightName;
        this.flightTotalSeats = flightTotalSeats;
    }

    public int getTotalNoSeats() {
        return flightTotalSeats;
    }

    public void setTotalNoSeats(int flightTotalSeats) {
        this.flightTotalSeats = flightTotalSeats;
    }

    public int getflightID() {
        return flightID;
    }

    public void setflightID(int flightID) {
        this.flightID = flightID;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }


}

