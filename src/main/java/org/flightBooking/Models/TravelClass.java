package org.flightBooking.Models;
import java.util.ArrayList;

public class TravelClass {
    private String flightClass;
    private int noOfSeatsForEachFlightClass;
    private int flightId;

    public int getflightId() {
        return flightId;
    }

    public void setflightId(int flightId) {
        this.flightId = flightId;
    }

    public String getSeatClass() { return flightClass; }

    public void setSeatClass(String seatClass) {
        this.flightClass = seatClass;
    }

    public int getSeatsAvaliable() {
        return noOfSeatsForEachFlightClass;
    }

    public void setSeatsAvaliable(int seatsAvaliable) {
        this.noOfSeatsForEachFlightClass = seatsAvaliable;
    }
    public TravelClass(){}
    public TravelClass(int flightId, String seatClass, int noOfSeatsForEachFlightClass) {
        this.flightId=flightId;
        this.flightClass = seatClass;
        this.noOfSeatsForEachFlightClass = noOfSeatsForEachFlightClass;

    }

}