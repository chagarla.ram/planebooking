package org.flightBooking.Controllers;

import org.flightBooking.Models.AjaxResponseBody;
import org.flightBooking.Services.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;

@RestController
public class FlightSearchController {

    @Autowired
    private FlightService repository;

    @GetMapping("/search")
    public String[] search(@RequestParam(required = true) String source, @RequestParam(required = true) String destination, @RequestParam(required = false, defaultValue = "1") int noOfPassengers, @RequestParam(required = false, defaultValue = "2019-08-21") String flyDate, @RequestParam(required = true) String selectedClass) {
        AjaxResponseBody result = new AjaxResponseBody();
        ResponseEntity.ok("hello");
        return(repository.getdispFlightDetails(source, destination, noOfPassengers, flyDate, selectedClass));
    }
}
