package org.flightBooking.Repository;

import java.util.*;
import org.flightBooking.Models.FlightData;
import org.flightBooking.Models.Flight;
import org.flightBooking.Models.FlightScheduledTravelClass;
import org.flightBooking.Models.TravelClass;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class FlightDataStaticRepository<repository> {
    private ArrayList<Flight> flight = new ArrayList<>();

    private TravelClass t1 = new TravelClass(1,"Economy",195);
    private TravelClass t2 = new TravelClass(1,"BussinessClass",35);
    private TravelClass t3 = new TravelClass(1,"FirstClass",8);
    private TravelClass t4 = new TravelClass(2,"Economy",144);
    private TravelClass t5 = new TravelClass(2,"BussinessClass",0);
    private TravelClass t6 = new TravelClass(2,"FirstClass",0);
    private TravelClass t7 = new TravelClass(3,"Economy",152);
    private TravelClass t8 = new TravelClass(3,"BussinessClass",20);
    private TravelClass t9 = new TravelClass(3,"FirstClass",0);

    private FlightData f1 = new FlightData(1,"Hyderabad", "Bangalore", "11:30 am", "1:00 pm", 6000, "2019-08-21", 200);
    private FlightData f2 = new FlightData(2, "Chennai", "Bangalore", "1:30 pm", "2:30 pm", 6200, "2019-08-21", 144);
    private FlightData f3 = new FlightData(3, "Delhi", "Bangalore", "10:30 am", "12:00 pm", 5500, "2019-08-21", 152);

    private ArrayList<FlightScheduledTravelClass> flightScheduledTravelClasslist = new ArrayList<>();
    private FlightScheduledTravelClass flightScheduledTravelClass = new FlightScheduledTravelClass(1,"Economy",95,6000);
    private FlightScheduledTravelClass flightScheduledTravelClass1 = new FlightScheduledTravelClass(2,"Economy",144,4500);
    private FlightScheduledTravelClass flightScheduledTravelClass2 = new FlightScheduledTravelClass(3,"Economy",155,5000);
    private FlightScheduledTravelClass flightScheduledTravelClass3 = new FlightScheduledTravelClass(1,"BusinessClass",35,13000);
    private FlightScheduledTravelClass flightScheduledTravelClass4 = new FlightScheduledTravelClass(2,"BusinessClass",20,14000);
    private FlightScheduledTravelClass flightScheduledTravelClass5 = new FlightScheduledTravelClass(3,"BusinessClass",0,0);
    private FlightScheduledTravelClass flightScheduledTravelClass6 = new FlightScheduledTravelClass(1,"FirstClass",8,20000);
    private FlightScheduledTravelClass flightScheduledTravelClass7 = new FlightScheduledTravelClass(2,"FirstClass",0,0);
    private FlightScheduledTravelClass flightScheduledTravelClass8 = new FlightScheduledTravelClass(3,"FirstClass",0,0);

    private ArrayList<FlightData> flightList = new ArrayList<>();
    private ArrayList<TravelClass> travelClassList = new ArrayList<>();
      public FlightDataStaticRepository() {
        Flight f4 = new Flight(1, "Boeing 777-200LR(77L)",238);
        Flight f6 = new Flight(2, "Airbus A319 V2",144);
        Flight f5 = new Flight(3, "Airbus A321",187);
        flight.add(f4);
        flight.add(f5);
        flight.add(f6);
        flightList.add(f1);
        flightList.add(f2);
        flightList.add(f3);

        travelClassList.add(t1);
        travelClassList.add(t2);
        travelClassList.add(t3);
        travelClassList.add(t4);
        travelClassList.add(t5);
        travelClassList.add(t6);
        travelClassList.add(t7);
        travelClassList.add(t8);
        travelClassList.add(t9);

        flightScheduledTravelClasslist.add(flightScheduledTravelClass);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass1);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass2);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass3);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass4);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass5);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass6);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass7);
        flightScheduledTravelClasslist.add(flightScheduledTravelClass8);
    }

    public List<FlightData> getAllFlightList() {

        return(this.flightList.stream().collect(Collectors.toList()));
    }
    public List<TravelClass> allFlightstravelClassList() {

        return(this.travelClassList.stream().collect(Collectors.toList()));
    }
    public List<Flight> allFlights() {

        return(this.flight.stream().collect(Collectors.toList()));
    }
    public List<FlightScheduledTravelClass> getallScheduledflightTravelClass(){
        return(this.flightScheduledTravelClasslist.stream().collect(Collectors.toList()));
    }
}
