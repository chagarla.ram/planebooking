package org.flightBooking.Services;

import org.flightBooking.Models.Flight;
import org.flightBooking.Models.FlightData;
import org.flightBooking.Models.FlightScheduledTravelClass;
import org.flightBooking.Models.TravelClass;
import org.flightBooking.Repository.FlightDataStaticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class FlightService {
    private static ArrayList<String> tmparray = new ArrayList<String>();

    @Autowired
    public FlightDataStaticRepository repository;

    public FlightService(FlightDataStaticRepository repository) {
        this.repository = repository;
    }

    public String[] getdispFlightDetails(String source, String destination, int passengers, String flyDate, String selclass) {

        List<FlightData> l = repository.getAllFlightList();
        List<Flight> l1 = repository.allFlights();
        List<TravelClass> tr = repository.allFlightstravelClassList();
        List<FlightScheduledTravelClass> fstc = repository.getallScheduledflightTravelClass();

        List<FlightData> newl = l.stream()
                .filter(i -> (source.equals(i.getSource()) && destination.equals(i.getDestination()) && (passengers <= i.getTotalPassengers())))
                .collect(Collectors.toList());
        int flightid1 = newl.get(0).getFlightId();

        List<Flight> newl1 = l1.stream()
                .filter(i -> (i.getflightID() == flightid1))
                .collect(Collectors.toList());
        String flightName = newl1.get(0).getFlightName();

        List<TravelClass> newtr = tr.stream()
                .filter(i -> (selclass.equals(i.getSeatClass()) && (i.getflightId() == flightid1)))
                .collect(Collectors.toList());
        if (selclass == "Economy") {
            int totalnoofseatsineconomy = newtr.get(0).getSeatsAvaliable();

        }
        List<FlightScheduledTravelClass> newfstc = fstc.stream()
                .filter(i -> (i.getFlightId() == flightid1) && (selclass.equals(i.getFlightClass())) && (passengers <= (i.getNoOfSeatsForFlightClassAvaliable())))
                .collect(Collectors.toList());
        double increbase = 1;

        if(selclass.equals("Economy")) {
            int seatsAvaliableineconomy = newtr.get(0).getSeatsAvaliable();
            int totalnoofseatsA = newfstc.get(0).getNoOfSeatsForFlightClassAvaliable();
            int currentbookingno = (seatsAvaliableineconomy-totalnoofseatsA)+passengers;

            if (currentbookingno <= ((40 * seatsAvaliableineconomy) / 100)) {
                increbase = 1;
            } else if ((currentbookingno <= ((90 * seatsAvaliableineconomy) / 100)) && (currentbookingno >= ((40 * seatsAvaliableineconomy) / 100))) {
                increbase = 1.3;
            } else {
                increbase = 1.6;
            }
        }
        String[] result = new String[2];

        if (newfstc.isEmpty()) {
            result[0] = "No Flights are avaliable for ";
            result[1] = "";
        } else {
            int seatPrice = newfstc.get(0).getPriceBasedOnClass();
            result[0] = flightName;
            result[1] = Double.toString(increbase * seatPrice);
        }

//        Object[] array = new Object[4];
//        array[0]= newl;
//        array[1] = newl1;
//        array[2] = newtr;
//        array[3] = newfstc;
        return(result);

    }
}