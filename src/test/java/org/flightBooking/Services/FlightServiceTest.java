package org.flightBooking.Services;


import org.flightBooking.Models.FlightData;
import org.flightBooking.Repository.FlightDataStaticRepository;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FlightServiceTest {
    private ArrayList<FlightData> fd ;


    @Test
    public void getdispFlightDetails() {
        FlightDataStaticRepository flightDataStaticRepository = mock(FlightDataStaticRepository.class);
        when(flightDataStaticRepository.getAllFlightList()).thenReturn(fd);
        FlightService fs = new FlightService(flightDataStaticRepository);
        Object[] result = fs.getdispFlightDetails("Hyderabad","Bangalore",1, "2019-08-21","Economy");
        assertEquals(fd,result);
    }


    @Test
    public void testGetdispFlightDetails() {
    }


    public Date createDate() {
        String sDate1="2019-08-21";
        Date date1;
            try {
                date1 = new SimpleDateFormat("YY-MM-DD").parse(sDate1);
               return date1;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        return null;
    }
}